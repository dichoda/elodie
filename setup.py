#!/usr/bin/env python

import codecs
import sys
import os
import unittest

from setuptools import setup, Command

with open('elodie/VERSION') as version_file:
    version = version_file.read().strip()


def list_files(dpath, prefix=''):
    """
        Args:
           dpath:  The subdirectory in which to find files
           prefix: The prefix to use for each subdirectory in dpath
            
        Returns:
           A list of directory + [files] tuples:
           [( prefix+d1, [f1, f2, f3] ), ( prefix + d2, [f4, f5] )]
    """
    return [(os.path.join(prefix, os.path.relpath(d, dpath)),
             [os.path.join(d, f)
              for f in files])
            for d, subdirs, files in os.walk(dpath)]


#class RunTests(Command):
#    user_options = []
#
#    def initialize_options(self):
#        pass
#
#    def finalize_options(self):
#        pass
#
#    def run(self):
#        loader = unittest.TestLoader()
#        tests = loader.discover('tests', pattern='*_test.py', top_level_dir='.')
#        runner = unittest.TextTestRunner()
#        results = runner.run(tests)
#        sys.exit(0 if results.wasSuccessful() else 1)


with codecs.open('Readme.md', 'r', 'utf-8') as fd:
    setup(
        name='elodie',
        version=version,
        description='An EXIF-based photo assistant, organizer, manager and workflow automation tool',
        long_description=fd.read(),
        author='Jaisen Mathai',
        maintainer='Douglas Pew',
        python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*,!=3.3.*,!=3.4.*',
        packages=['elodie',
                  'elodie/media',
                  'elodie/plugins',
                  'elodie/plugins/runtimeerror',
                  'elodie/plugins/googlephotos',
                  'elodie/plugins/throwerror',
                  'elodie/plugins/dummy',
                  'elodie/external',
                  'elodie/tools'],
        include_package_data=True,
        package_data={
            'elodie': [
                'VERSION',
            ],
        },
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: End Users/Desktop',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: 2',
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.6',
            'Topic :: Multimedia',
            'Topic :: Multimedia :: Graphics',
            'Topic :: Multimedia :: Graphics :: Capture',
            'Topic :: Utilities',
        ],
        entry_points={
            'console_scripts': [
                'elodie = elodie.main:cli',
            ],
        }
    )

