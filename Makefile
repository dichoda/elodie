#
#  Makefile Targets
#
#   MAIN
#
#   all:        Runs checks and builds RPM
#   clean:      Deletes build products
#   check:      Runs all checks (unit tests, code formatting, lint)
#   yapf-apply: Applys yapf python code formatter
#   elodie:     Installs elodie locally in env/
#
#   TESTS
#
#   yapf:       Verifies python code formatting with YAPF
#   tox:        Run python Tox tests
#
#   MISC
#
#   tar:        Creates WARR source tarball
#   env:       Initializes python virtualenv (env/)
#
   -include settings.mk

   RM = /bin/rm -rf --preserve-root

   MAVEN_GROUP=xag.jicd
   YAPF_STYLE=google

   ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
   VERSION:=$(shell cat $(ROOT_DIR)/VERSION)

   TARBALL=${RPMNAME}-${VERSION}.tar.gz

   PYTHON_SOURCES=elodie setup.py

.PHONY: all clean check tar noenv

all:: check elodie-init
tar: noenv dist/${TARBALL}

dist/${RPMFILE}: dist/${TARBALL} ${SPECFILE}
	mkdir -p build/{BUILD,RPMS/{i{3,4,5,6}86,x86_64,noarch},SOURCES,SPECS,SRPMS}
	cp dist/${TARBALL} build/SOURCES/.
	cp ${SPECFILE} build/SPECS/.
	rpmbuild -ba --define '__version ${VERSION}' --define '_topdir ${ROOT_DIR}/build' --define '__rpmrelease ${RPMRELEASE}' ${SPECFILE}
	cp build/RPMS/noarch/${RPMFILE} dist/

dist/${TARBALL}: elodie/VERSION VERSION setup.py
	python setup.py sdist

clean::
	$(RM) build dist ${RPMNAME}.egg-info

noenv:
	@if [ -n "$$VIRTUAL_ENV" ];  then \
		echo "ERROR: Building RPMs WILL NOT WORK in a python virtual environment." 1>&2; \
		echo "       Use 'deactivate' to terminate the current virtual env." 1>&2 ; \
		echo "       See https://virtualenv.pypa.io/en/latest/userguide/#index-0" 1>&2 ; \
		echo "" ;\
		exit 1; \
	fi

# These targets synchronize VERSION file
# warr needs VERSION file, but is kept in root directory
.PHONY: version clean check

version: elodie/VERSION

elodie/VERSION: VERSION
	cp VERSION elodie/VERSION

clean::
	$(RM) elodie/VERSION

check:: lint

# Python Targets (wrapped in Virtual Env)
#
.PHONY: env elodie elodie-init tox yapf yapf-apply check clean

env: env/bin/activate

env/bin/activate: requirements.txt
	test -d env || virtualenv env
	. env/bin/activate; python -m pip install --upgrade pip
	. env/bin/activate; pip install -Ur requirements.txt
	touch env/bin/activate

elodie elodie-init: env version
	. env/bin/activate; python setup.py install --

tox: version env
	    . env/bin/activate; tox --skip-missing-interpreters=true

yapf: env
	exit 0;. env/bin/activate; yapf --style=$(YAPF_STYLE) --recursive --diff $(PYTHON_SOURCES)

yapf-apply: env
	exit 0; echo . env/bin/activate; yapf --style=$(YAPF_STYLE) --recursive --in-place $(PYTHON_SOURCES)

check:: yapf tox

clean::
	$(RM) env .tox

